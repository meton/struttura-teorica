# Sistema abilità
Le abilità sono 5 per dottrina, per un totale di 30 abilità complessive. Si è optato per un numero esiguo per 2 motivi:  
1. Abbellire un personaggio con venti abilità diverse porta ad avere troppe input e al darsi al semplice premere tasti a caso (o a essere imboccati stile TERA o Blade&Soul). Un giocatore dovrebbe invece aver ben chiaro cosa fanno le sue abilità e capire come usarle in modo strategico
2. Al tempo stesso un giocatore deve avere chiare le abilità degli altri personaggi, per esempio in un combattimento: meno abilità ci sono da ricordarsi e meno sforzi si fanno nel memorizzarle (sia anche solo visivamente)

## Stadi e livelli
Un’abilità si divide in 4 stadi: normale, maestro, gran maestro, eccelsa. Ognuno di questi stadi è suddiviso in 10 livelli, tranne “eccelsa”. Quando si passa da uno stadio all’altro vi è un incremento sostanziale degli attributi e un cambio grafico.

### Incremento livelli abilità
A seconda dello stadio, un’abilità può essere aumentata in maniera diversa.
- **Normale**: ogni volta che si sale di livello, si ottiene un punto investibile sulle skill normali. Al mettere il decimo punto, la skill passerà a Maestro
- **Maestro**: necessita della lettura del libro abilità di quella skill. Dopo aver letto con successo 10 libri, la skill passerà a Gran Maestro
- **Gran Maestro**: necessità della lettura del tomo di quella skill. Dopo aver letto con successo 10 tomi, la skill passerà a Eccelsa

Ogni passaggio da un livello all’altro incrementa leggermente le statistiche dell’abilità, incentivando quindi non solo il salire di stadio ma anche quello di livello

## Libri e tomi abilità
I libri e i tomi funzionano nella medesima maniera: sono oggetti che spariscono dopo l’uso, assegnando un punto alla skill di cui portano il nome (per esempio: “Essere acqua libro abilità” sarà leggibile da una sacerdotessa di giada con Essere acqua a Maestro).

### Possibilità di successo di lettura
Per disincentivare le persone che vogliono accelerare la crescita di un secondo account/personaggio, la possibilità di leggere con successo un libro o un tomo variano a seconda della frequenza.  

Al primo utilizzo un libro ha il 100% di possibilità di successo. La lettura applica per 24 ore effettive lo status “lettura concentrata”, che abbasserà del 20% la possibilità di successo di un secondo libro finché lo status non scade. Se si leggesse un secondo libro con successo, il timer di 24 si resetterebbe, abbassando di un ulteriore 20% la possibilità di successo. Se fallisse, non cambierebbe nulla. Il libro/tomo sparisce in ogni caso.  

Questo per dire, che basta avere i giocatori che impostano un costo medio-alto sui libri e sui tomi per dissuadere i più dal leggere più di 2, 3 libri al giorno.
### Dove ottenerli
I libri possono essere ottenuti esclusivamente dalle [schegge](https://gitlab.com/meton/struttura-teorica/blob/master/PvE/Schegge%20corrotte.md)

[DA VEDERE PER I TOMI: metin lv. alto, boss ecc.]
