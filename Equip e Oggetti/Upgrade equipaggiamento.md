# Upgrade equipaggiamento

I vari pezzi di equipaggiamento spaziano da +0 a +9 e il loro livello può essere incrementato dall'NPC del Fabbro tramite soldi ed eventuali oggetti aggiuntivi. In natura (ovvero uccidendo i nemici) si trovano da +0 a +2.

## Possibilità di upgrade
Al contrario dell'originale Metin 2, gli oggetti NON hanno possibilità di fallire nell'upgrade. Questo perché l'upgrade dovrebbe essere meritato (e sudato), non frutto di un valore randomico. Ragionamento base: siete più soddisfatti a uccidere un boss di Dark Souls o a essere stati fortunati nel vincere qualcosa? Nel primo caso ve lo siete meritato, nel secondo poteva succedere a voi come a chiunque altro.

## Requisiti per l'upgrade
A parte l'equipaggiamento lv.5 e lv.10, tutti gli altri si basano sul seguente schema:

+1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9
--- | --- | --- | --- | --- | --- | --- | --- | ---
soldi | soldi | soldi | soldi + item comune | soldi + item comune | soldi + item raro | soldi + item schegge dell'altro regno | soldi + item boss di zona | soldi + item boss di zona dell'altro regno

dove "soldi" raddoppia ogni volta. Gli item lv.5 e lv.10 invece non richiedono item di boss, scalando il tutto di 2 posizioni verso destra (inizierà a chiedere oggetti dal +6).  

> Si potrebbe obiettare che un item comune o un item raro siano comunque randomici da ottenere, ma c'è comunque una netta separazione tra gli oggetti fino a +6 e quelli da +7 a +9 (che vengono droppati al 100% ma che sono molto più impegnativi da ottenere). Questo permette di giocare "casual" a chi vuole e di impegnarsi al massimo per chi vuole l'equip più potente e performante
