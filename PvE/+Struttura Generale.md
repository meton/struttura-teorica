# Struttura generale

A differenza di molti MMORPG, l'approccio con i nemici differisce in una maniera molto semplice: i nemici sono perlopiù a gruppi.  

## Gruppi di nemici
I nemici che compongono un gruppo possono variare da 2 fino a un massimo di 5 membri, e basta colpirne uno per provocare anche gli altri. Questo comporta uno stile di combattimento più ignorante e soddisfacente (Dynasty Warriors insegna), dove colpendo 3 nemici di 3 gruppi diversi, ci si ritrova con una dozzina di mob alle calcagna. Ovviamente i colpi fisici delle varie classi - escluso il ninja arco - sono ad area, permettendo di colpire quindi più nemici possibili.  

## Mobbare
Neologismo per dire "raggruppare nemici (mob)", è una tecnica usata per expare più velocemente. Spinge a collaborare tra i giocatori, avendo solitamente un ninja (l'unico in grado di poter usare attacchi a distanza) che porta al gruppo i mostri. Questo rende il ninja arco - che ha un'abilità per andar più veloce - la dottrina più adatta allo scopo, compensando con il fatto che expando in solitaria farebbe più fatica.  
Inoltre, questo spinge le persone a contendersi pezzi di mappa, soprattutto nelle zone neutrali dove si incrociano i regni. Portando di conseguenza a risse e/o accordi di quieto vivere tra i giocatori.

## Aggro
L'aggro (ovvero un valore che determina contro chi punterà il mostro) è un valore condiviso in un gruppo, che incrementa man mano che vengono inflitti colpi ai nemici. L'unico modo per modificare l'aggro senza colpire i nemici è la skill Urlo di Battaglia del Guardiano.  
[DA FINIRE: formule per calcolarla, vedere se fare in modo che solo un mob di un gruppo possa cambiare target mentre gli altri tengono quello originale ecc.]
