# METON - Struttura Teorica

Il gioco è ispirato a Metin 2, con la piena consapevolezza dell'autore di quanto il gioco originale fosse limitato, basato su grind estremo e fortemente orientato verso il pay to win. L'idea è perciò quella di prenderne solo i lati positivi nel tentativo di costruire un MMORPG valido (nome ancora da definire).  

## Caratteristiche fondamentali dello sviluppo
- **software libero**: gioco open source con licenza GPL 3: chiunque è libero di contribuire e/o clonarlo per una versione propria affinché mantenga la stessa licenza
- **engine libero**: il motore di gioco deve seguire la stessa regola: Godot con il suo linguaggio nativo GDscript. Che al contrario di Unity e C# non contiene telemetria
- **niente meccaniche che creano dipendenza**: il giocatore deve essere libero di voler aprire il gioco, e non essere spinto da ricompense giornaliere o gioco d'azzardo
- **niente raccolta dati**: al contrario di praticamente tutti i giochi online, non si ha intenzione di raccogliere dati e/o rivenderli a terze parti. Il gioco è tempo per sé, non per le aziende
- **niente profitto in cambio di vantaggi**: un gioco deve misurare le abilità di un giocatore, non la capienza della sua carta di credito
- **espandibile**: è un gioco fatto a tempo perso. Un modello Minecraft che va via via ad espandersi è più salutare. È il succo che deve funzionare, non solo l'insieme
- **semplice**: il giocatore deve sempre aver ben chiaro il funzionamento del gioco. Niente giochi che richiedono conoscenze di interi manuali o con ventimila feature inutili
- **sociale**: un gioco online non ha senso di esistere se non viene evidenziata la componente sociale, sia essa competitiva o cooperativa

## Caratteristiche base del gioco
- Soddisfazione nel combattimento
- Pochi contenuti ma buoni
- Ogni fase del gameplay deve essere autonoma E bilanciata nell'insieme

## Come contribuire
Hai un'idea? Suggeriscila facendo un account su GitLab e
1. Se sei esperto [clona il progetto](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) e suggerisci le modifiche con un [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
2. Se non lo sei, apri una issue [qui](https://gitlab.com/meton/struttura-teorica/issues) descrivendo ciò che implementeresti o che non ti convince. Usa un titolo chiaro!