# Chat

La chat mette in comunicazione i giocatori e può segnalare loro determinati eventi, come la quantità di soldi raccolti o l'exp ottenuta. 

## Tipi di chat
Le chat, identificabili con colori diversi e accessibili con diverse shortcut (vedere sotto), si dividono in:

chat | colore | shortcut | descrizione
---|---|---|---
locale | bianca | | mostra i messaggi dei giocatori intorno a sé (misura da vedere)
globale | verde | ! | visibile da tutti. Non utilizzabile sotto il livello 10 e con un delay minimo di 10 secondi tra un messaggio e l'altro
gruppo | azzurra | # | visibile solo dal proprio party
gilda | gialla | % | visibile solo dalla gilda
sistema | grigia | | visibile dal singolo giocatore. In questa chat non è possibile scrivere

Per cambiare la chat in cui si sta scrivendo di base si può selezionarla dall'interfaccia. Oppure...

### Shortcut

Le shortcut servono a inviare più facilmente un messaggio in una determinata chat senza clic inutili. Per esempio, per scrivere ```ciao a tutti``` nel gruppo, essendo la sua shortcut ```#```, basterà scrivere ```#ciao a tutti``` (il # non viene visualizzato all'invio, serve solo al sistema per interpretarlo come un "questo va nella chat del gruppo")

## Comandi chat
La chat dispone anche di rapidi comandi. Il loro simbolo è ```&```.

comando | funzione
--- | ---
&coord | invia un testo cliccabile in chat con le proprie coordinate
&roll | genera un numero casuale da 1 a 100

### &coord

```&coord``` è molto utile per comunicare la propria posizione, magari perché si è avvistato una scheggia, un giocatore nemico o perché un amico vuole raggiungerci.  

Il messaggio visualizzato in chat equivale a ```Luogo(X,Y)```, per esempio ```Valle(105,351)```. Se un giocatore si trova nella mappa corrispondente, può cliccare sul testo e il gioco segnalerà sulla mappa il punto inviato.

### &roll

```&roll``` è utile quando si vuole prendere una decisione in modo del tutto casuale. Per esempio, se c'è rimasto solo un posto in un gruppo e due persone vogliono entrare si può scrivere ```Tizio: &roll, Caio: &roll``` e far entrare chi ottiene il numero più alto.  
Il font del numero sarà ovviamente diverso dallo standard per evitare che la gente scriva numeri di propria mano per poi dire che li ha rollati.
