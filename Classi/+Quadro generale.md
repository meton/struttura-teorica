3 classi con 2 dottrine per classe (per un totale di 6): guerriero, ninja e sacerdotessa. Inizialmente avranno solo un sesso, ovvero guerriero e ninja maschi, sacerdotessa femmina. (sarebbe comunque interessante un ninja dal sesso non identificabile per bilanciarlo al meglio). Le loro caratteristiche base sono:  

Guerriero > forza  

Ninja > velocità  

Sacerdotessa > magia  

Queste caratteristiche diventano ancora più specifiche a seconda della loro dottrina, ovvero:  

| Classe       | Dottrina   |  Caratteristica          | Arma                    |
|--------------|------------|--------------------------|-------------------------|
| [Guerriero](https://gitlab.com/meton/struttura-teorica/blob/master/Classi/Guerriero.md)    | Lanciere*  | Danno elevato            | Spadone                 |
|              | Colosso  | Tank con alto CC         | Mazza o guanti ([tipo](https://gamepedia.cursecdn.com/smite_gamepedia/d/da/SkinArt_Hercules_Default.jpg?version=efa9ca0402cf0ab544160b7533a92951))** |
| [Ninja](https://gitlab.com/meton/struttura-teorica/blob/master/Classi/Ninja.md)        | Miraggio   | DPS                      | Falcetti                |
|              | Arco       | Combattimento a distanza | Arco                    |
| [Sacerdotessa](https://gitlab.com/meton/struttura-teorica/blob/master/Classi/Sacerdotessa.md)* | Giada      | Supporto                 | Ventaglio               |
|              | Drago      | Danno concatenato        | Campana Magica ([tipo](https://it-wiki.metin2.gameforge.com/images/3/39/Immagine_Campana_di_Giada.png))   |

*nomi provvisori  
**da decidere  

Ogni classe dispone di 5 abilità, nel tentativo di bilanciarle tra PvP e PvM. Alcune classi tendono più a uno e altre più all’altro, ma evitando di creare divari troppo ampi.
