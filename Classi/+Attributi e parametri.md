# Attributi e parametri

## Differenza
Un Attributo è un valore che può essere aumentato permanentemente tramite punti attributi da assegnare (acquisibili accumulando esperienza) e temporaneamente tramite equipaggiamento (finché si ha un certo oggetto equipaggiato). Un parametro, al contrario, è influenzato dagli attributi e da eventuale equipaggiamento.

## Attributi
Gli attributi sono 4:
- VIT - ovvero vitalità. Incrementando questo attributo, aumentano HP massimi, difesa fisica e magica
- INT - ovvero intelligenza. Aumenta MP massimi e danno magico
- STR - ovvero forza (*strength*). Aumenta il danno fisico
- DEX - ovvero destrezza (*dexterity*). Aumenta la possibilità di schivare un colpo fisico, la velocità di attacco e in minima parte quella di movimento

NOTA BENE: anche la velocità di cast può essere aumentata, con una formula (da definire) che combina INT e DEX.  

Al contrario di molti giochi con 9-10 attributi, anche qui si è voluto puntare sulla semplicità: basandosi su questi 4 valori, si possono benissimo creare accoppiate che funzionano bene con certe dottrine e altre che funzionano male. E al tempo stesso il loro funzionamento è facilmente comprensibile.

### Aumentare gli attributi
Ogni livello permette di ottenere 3 punti attributi (al 25%, 50% e 75% di exp), assegnabili a piacere del giocatore. Tuttavia, un attributo non può superare il valore 60, questo vuol dire che al cap massimo (lv.50) sarà possibile aver maxato solo 2 attributi.

#### Incrementi aggiuntivi
Per incrementare ulteriormente il valore massimo, si possono usare pezzi di equipaggiamento che ne aumentino il valore. Per esempio, se STR è al 60 e abbiamo un'arma con STR+5, la nostra STR sarà 65.

## Parametri
I parametri principali sono quelli incrementabili tramite attributi, ovvero:
- HP
- MP
- Attacco fisico
- Attacco magico
- Difesa fisica
- Difesa magica
- Velocità attacco
- Velocità cast
- Velocità movimento
- Schivata

[DA FINIRE eventuali formule approfondite e spiegazioni nel dettaglio]

### Parametri secondari
[DA FINIRE]


