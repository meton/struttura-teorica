# Protezione PvP

## Protezione
La protezione al PvP consiste nel rendere completamente immune un giocatore da qualsiasi tipo di attacco, status alterato e skill.

## Dove proteggersi
Ci sono diversi modi per evitare di essere picchiati per la mappa.

### Safe zone
Le safe zone sono quelle zone sicure dove niente e nessuno può scalfirci. Le safe sono uguali per tutti, indipendentemente dai regni. Sono di due tipi:
- la piazza del villaggio
- le zone limitrofe agli accessi tra una mappa e l'altra

### Protezioni ai livelli bassi
Onde evitare comportamenti quali i server faction di Minecraft dove la gente ti attende all'uscita della safe zone armata fino ai denti, una protezione dal PvP per chi inizia è necessaria. Essendo il cap il 50, una protezione fino al livello 14 può essere adeguata.

#### Invasione dei regni
Se, tuttavia, una persona sotto al lv.15 decide di andare in un altro regno, finché rimane nel tale è esposta al PvP.
