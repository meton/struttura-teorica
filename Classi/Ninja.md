# Ninja
Il ninja è la classe per chi vuole avere più mobilità. Rappresenta l’evasione

## Miraggio
Il ninja miraggio è quello che ti uccide senza neanche darti il tempo di realizzare dell’assalto. E, se sopravvivi, ne uscirai probabilmente con status alterati. La rapidità e l’inganno sono i suoi punti chiave, grazie alla sua abilità Miraggio dell’ombra

#### Abilità
**Miraggio dell’ombra**  
Crea un clone di se stesso, diventando invisibile per qualche secondo e guadagnando velocità di movimento. Se il clone viene colpito, emana una nube tossica AoE che avvelena l’avversario. Il clone eseguirà l’azione di quando è stato castato (ovvero se stavo camminando prima di far partire la skill, continuerà a camminare in quella direzione). Esplode in automatico dopo 5 secondi. Eventualmente, lo si può usare direttamente come nube velenosa tenendo premuta la skill quando la si casta  

**Assalto fulmineo**  
Skill a distanza: si teletrasporta nel punto prestabilito, infliggendo danno medio-basso AoE e atterrando gli avversari (knockdown)  

**Catena del tormento**
Invoca un palo di legno vicino a sé, scagliando una catena in linea retta. Se la catena afferra qualcuno, lo tira a sé sbattendolo contro al palo. Danno elevato, target singolo. Countera tutte le skill  

**Rottura dei garretti**
Attacco a corta distanza a ventaglio AoE, dove il ninja si accovaccia per colpire alle gambe. Danno medio-alto, % di sanguinamento  

**Danza della mietitura**  
Il ninja volteggia in linea retta colpendo e attraversando tutti i nemici sul percorso  
<br>
<br>  
<br>  

## Arco
Il ninja arco predilige le distanze: è la classe più scoraggiata nel PvM in quanto la sua arma è l’unica a non colpire AoE. Tuttavia, compensa nel PvP dove ha un buon controllo sulla mappa e un danno elevato. Le frecce vanno direzionate dal giocatore, non è mira automatica

#### Abilità
**Trappola pesata**  
Posiziona una trappola pesata sul terreno che immobilizza chiunque ci passi sopra. Ne può posizionare per un massimo di 3 e scadono dopo 5 minuti o quando si disconnette. Piccola % sanguinamento  

**Triplo scocco**  
Caricando ulteriormente l’arco, i prossimi 3 colpi applicheranno knockback singolo e piccola % rallentamento. Se non lanciati entro 6 secondi, scadono  

**Schivata perforante**  
Fa un roll nella direzione in cui sta camminando (se è fermo lo fa in avanti) e carica una freccia perforante e più larga di una normale (tipo Varus di League of Legends). Piccola % di dimezzare difesa  

**Passo piumato**  
Boost che aumenta la velocità di movimento  

**Passo delle punte di corvo**  
Lancia a terra una manciata di punte di corvo che provocano danni per secondo al passarci sopra e rallentamento  
