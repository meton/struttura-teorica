# Sacerdotessa
La sacerdotessa è per chi vuole una vibe più esoterica. Rappresenta la magia

## Giada
La sacerdotessa di giada è il support per eccellenza. Anch’essa fatica ad expare in quanto i suoi danni non sono elevatissimi (a meno che non la si buildi full danno, perdendo la capacità da support), tuttavia ha un buon controllo della situazione, tanto da poter rendere inutile qualsiasi abilità avversaria se attiva Essere acqua al momento giusto

#### Abilità
**Essere acqua**  
Diventa immune a qualsiasi tipo di danno e status alterato per 1.5 secondi. Se ha qualche status alterato, scompare  

**Aiuto alla battaglia**  
Aumenta la % di critici. Può essere castata su qualsiasi giocatore, dura ca. 1 minuto  

**Nebbia della piana celeste**  
Aumenta la % di evadere un colpo. Può essere castata su qualsiasi giocatore, dura ca. 1 minuto  

**Ascendere la corrente**  
Attacco AoE dove evoca da un grande cerchio ai suoi piedi delle colonne acquatiche. Danno + knockair  

**Colpo della tartaruga d’acqua**  
Lancia una massa d’acqua a forma di tartaruga in linea retta che attraversa ogni nemico. Danno + rallentamento  
<br>
<br>  
<br>  


## Drago
La sacerdotessa del drago è un ibrido basato sulla concatenazione di attacchi: più ti prende e più fa male grazie alla sua abilità Irradiare che, sommata a Richiamo dello spirito, può portare a ingenti danni. Più portata al PvM, manca infatti di vie di fuga, ma rimane indispensabile grazie a Sigillo del senzavoce

#### Abilità
**Richiamo dello spirito**  
Ogni abilità della sacerdotessa del drago stacka fiamme sull’avversario per un massimo di 10. Ad attivare Richiamo dello Spirito, l’avversario riceve un danno che incrementa a seconda delle stack.. Se ha 5 o più stack, subisce fuoco duraturo, se ha 10 stack viene anche stunnato. Scadono dopo 10 secondi dall’ultimo colpo  

**Sigillo del senzavoce**  
Stack fiamma: 1  
Silence completo di 5 secondi. Disattiva tutti gli status benefici che ha l’avversario  

**Irradiare**  
Stack fiamma: 1 per colpo  
Abilità da attivare e disattivare a piacimento. Quando disattivata, aumenta velocità movimento. I nemici colpiti con Irradiare attiva, perdono -3% difesa magica, per un massimo di 5 stack. Scadono dopo 5 secondi dall’ultimo colpo  

**Colpo testa di drago**  
Stack fiamma: 3  
Evoca una testa evanescente di drago, con meccanica simile al Colpo della tartaruga d’acqua. Danno + % fuoco duraturo  

**Vortice del drago**  
Stack fiamma: 2  
Vortica su se stessa scatenando un uragano AoE che  fa danno e applica knockair + knockback ai nemici. Piccola % fuoco duraturo
