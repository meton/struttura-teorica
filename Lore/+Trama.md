# Trama

Il mondo di gioco è diviso in tre regni separati da zone neutrali, dove ogni regno venera una divinità diversa. Nei regni precipitano di tanto in tanto pezzi di statue sacre, dove ognuno sostiene che i miscredenti degli altri regni stiano uccidendo la propria e stiano sgretolando il loro credo; questo peccare dei miscredenti avvelena le sacre statue situate sul monte più alto della regione, esattamente al centro delle zone neutrali (che sta in mezzo ai regni), facendo precipitare parti di esse (le schegge) nel mondo di gioco e contaminando i loro dintorni: gli animali diventano aggressivi, bestie ultraterrene prendono vita, costringendo gli abitanti a doverle distruggere per fermare il processo. Come fermare tutto ciò?  

(L'autore ha già un finale ben chiaro)

