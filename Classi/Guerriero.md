# Guerriero
Il guerriero è la classe per chi vuole menare d’ignoranza. Rappresenta la forza bruta  

## Lanciere
> Questa è la dottrina più simile a quella di Metin 2 per un semplice motivo: era OP se paragonata alle altre. Al posto di debuffare questa, si è preferito potenziare le altre. I nomi delle skill PER ORA sono rimasti i medesimi, ma vanno assolutamente cambiati. Frangiscudo è l'unica davvero originale

Questa dottrina si basa su danni elevati e una buona resistenza fisica. Grazie a Sibilare può coprire brevi distanze in un batter d’occhio, usandola sia per ingaggiare che per scappare

#### Abilità
**Aura della spada**  
Applica un boost sull’attacco a sé stesso per ca. 50 secondi  

**Sibilare**   
Raddoppia la sua velocità di movimento per una manciata di secondi. Se impatta contro un nemico, lo stunna. Danno medio  

**Gorgo delle lame**  
Ruota su sé stesso impugnando lo spadone come se dovesse lanciarlo, colpendo AoE. Danno elevato  

**Frangiscudo**  
Affonda lo spadone, danno medio-basso, con % di dimezzare la difesa nemica  

**Estasi da combattimento**  
Riduce la difesa fisica, incrementando la velocità d’attacco
<br>  
<br>  


## Colosso
Questa classe è progettata per chi vuole rendere la vita in PvP di gruppo un inferno: 4 CC pensati per non lasciar via di fuga all’avversario, che permettono tuttavia di expare in serenità in quanto causano anche danni  

#### Abilità
**Mitigare**  
applica un boost sulla difesa e lo rende immune a knockair e knockdown per ca. 60 secondi  

**Carica zampa di toro**  
Carica in linea retta nella direzione in cui guarda la telecamera. Tenendo premuto il tasto della skill fino a un massimo di 3 secondi, la gittata aumenta progressivamente ([tipo](https://invidio.us/watch?v=AqATrDFARK0)). Applica danni e knockair a chiunque sia sul suo tragitto e non si ferma all’impatto. A termine corsa stunna ad AoE (se si ferma esattamente su un personaggio, knockair + stun ridotto all’atterraggio)  

**Urlo di battaglia**  
Richiama a sé l’aggro dei mostri nelle vicinanze. Se c’è un player avversario, il player gli camminerà in contro per un secondo (il raggio per i player è più piccolo rispetto a quello dell’aggro)  

**Zampata del lone**  
Crepa il terreno circostante, causando danni, applicando knockair e attirando leggermente verso se stesso (il centro)  

**Mano del colosso**  
Si fa prima a mostrare l'esempio, minuto [2:35](https://invidio.us/watch?v=zpElXCJfiK8)
