# Status alterati

Come in ogni MMORPG, ci sono vari status alterati che possono essere applicati tra giocatori, nemici e boss; salvo protezioni e resistenze varie.

- **Mutismo**: il bersaglio non può usare skill di alcun tipo
- **Rallentamento**: la velocità del bersaglio è dimezzata
- **Stordimento**: il bersaglio rimane immobile e non può eseguire nessun'azione finché lo status è attivo
- **Radici**: come Stordimento, ma permette di eseguire qualsiasi azione sul posto
- **Sanguinamento**: danno per secondo che rallenta del 10%
- **Veleno**: danno per secondo
- **Fuoco duraturo**: danno per secondo
- **Rottura scudo**: difesa dimezzata

## Protezioni dagli status alterati

Parlando di nemici, progredendo col gioco essi (nemici comuni e boss) saranno sempre più immuni a determinati stati. Parlando di giocatori invece, non sarà possibile avere una protezione (ovvero un'immunità) dagli status alterati.

## Resistenze agli status alterati

Al contrario, un giocatore può avere uno o più equip che aumentano di una certa percentuale la resistenza a uno status alterato: se si parla di danni per secondo (```Sanguinamento```, ```Veleno``` e ```Fuoco Duraturo```) e di ```Rottura Scudo``` la resistenza riduce la possibilità di ricevere lo status, se si parla di crowd control (```Stordimento```, ```Rallentamento```, ```Radici```) si riduce la lunghezza. Knock-air, knoc-kup e ```Mutismo``` invece non hanno resistenze.

Il primo gruppo (danni per secondo e ```Rottura Scudo```) hanno resistenze per singolo malus, ovvero ```Resistenza Sanguinamento```, ```Resistenza Veleno``` e via dicendo. Queste hanno un massimo del 75%. Al contrario, i malus che rientrano in crowd control hanno una resistenza unica: ```Resistenza al Crowd Control```, per un massimo del 50%.

## Incremento status alterati

Come è possibile aumentare le resistenze, è anche possibile incrementare gli effetti di alcuni status alterati. I danni per secondo aumentano il danno e ```Rallentamento``` riduce ulteriormente la velocità. Gli altri status non possono essere incrementati tramite bonus.

