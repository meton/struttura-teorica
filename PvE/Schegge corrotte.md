# Schegge corrotte

## Storia
Queste schegge si sgretolano dalle statue sul monte più alto della regione, situato al centro delle zone neutrali che separano i tre regni. A causa della miscredenza degli abitanti, la mancanza di fede pare contaminare le statue fino a romperle. Le schegge precipitano per tutta la regione, avvelenando a loro volta ciò il territorio circostante: gli animali impazziscono, creature esoteriche prendono vita. Per questo motivo vanno distrutte.

## Tipi di schegge
Le schegge rappresentano 8 parti del corpo più il loto, dall'ampio simbolismo religioso. La lista è per ora in ordine sparso.
- Mano
- Piede
- Occhio
- Bocca
- Orecchio
- Testa
- Cuore
- Stomaco
- Loto

Partendo con il primo al livello 5 e mettendone uno ogni 5 livelli, si riesce a distribuirli equamente fino al 50, cap massimo. Tuttavia, potrebbe essere interessante aggiungerne un'ulteriore tipologia rappresentante le tre "essenze" delle tre divinità (magari stringendo il distacco di livello delle ultime schegge).

## Meccaniche
Una scheggia non è che un mostro passivo che funziona da spawner di nemici del suo livello. Ogni volta che la sua vita cala del 10%, invoca attorno a se delle ondate di nemici, che si fanno via via più consistenti fino all'ultima ondata (90% di vita persa), per un totale di 9 ondate. I nemici condivideranno la stessa aggro della scheggia, attaccando quindi chi sta causando più danni.

### Cambio grafico
Sarebbe interessante fare che dopo una certa vita persa (es. il 50%), l'aspetto della scheggia cambiasse in maniera tetra per simboleggiare il male che racchiude. Per esempio, un occhio può atterrare chiuso e spalancarsi terrorizzato durante la transizione, con capillari visibili e cambio di colore. Oppure una testa può perdere metà pelle e mostrare il cranio sottostante.

## Luoghi di spawn
Le schegge cadono in aree prestabilite della mappa e c'è un limite di spawn per evitare un sovraffollamento. Queste aree sono coerenti con il livello dei nemici presenti nella mappa

## Drop
Tra i drop più importanti, svettano sicuramente i libri abilità per aumentare il livello delle skill a maestro. DA FINIRE

### Oggetti per il +7
Qualsiasi item +7 (o +9 se parliamo di armi lv.5 e lv.10) necessita di distruggere una scheggia nell'altro regno del livello corrispondente. L'oggetto viene droppato al 100% SE si sono inflitti almeno il 50% dei danni alla scheggia e se il proprio livello rientra in un distacco massimo di 5 rispetto alla scheggia.
