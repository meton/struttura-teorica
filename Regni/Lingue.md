# Lingue

Le lingue rappresentano una delle abilità secondarie del personaggio. Per allenarle c'è bisogno del Manuale delle Lingue e dei Racconti del corrispondente, perché di base i giocatori di regni diversi non si possono capire.

## Apprendimento
L'apprendimento consiste in 36 livelli e si divide in due fasi:
- Fase 1: il giocatore vedrà caratteri strani non corrispondenti a nessuna logica. È impossibile capire cosa sta dicendo l'altro.
- Fase 2: il giocatore vedrà lettere non corrispondenti a quelle originali (cifrario a sostituzione), come [l'albhed di Final Fantasy X](https://www.allgamestaff.altervista.org/final-fantasy-x/dizionari-albhed/)

I primi 10 livelli rientrano nella fase 1, dove mano a mano i caratteri strani verranno sostituiti con delle lettere dell'alfabeto latino (sempre randomiche).  
I seguenti 26 livelli invece corrispondono alle lettere dell'alfabeto.

### Linguista del regno
Per iniziare a imparare le lingue, bisogna prima di tutto recarsi dall'NPC Linguista del regno del quale vogliamo imparare la lingua. Senza capire nulla, ci darà un Manuale lingue per Principianti: cliccandoci sopra possiamo ora farle salire di livello, partendo dal livello 1 (quindi qualche lettera sarà convertita già in alfabeto latino).  

Torneremo poi dal linguista all'11, dove ci assegnerà una missione dove dovremo parlare con alcuni NPC del suo regno per poi tornare da lui: completando la missione la lingua passerà al livello 12.

### Manuale delle lingue
I manuali delle lingue sono oggetti monouso che permettono di arrivare fino al livello 11.  

> chi li droppa? Schegge? Qualsiasi nemico random a percentuali molto basse? Vedere la issue https://gitlab.com/meton/struttura-teorica/issues/5

### Racconti di |nome-regno|
Questi sono i manuali avanzati, che permettono di arrivare sino al livello 26.  

> Stesso problema di sopra per i drop, issue https://gitlab.com/meton/struttura-teorica/issues/5

## Comprensione
Se due giocatori di regni diversi si parlano e nessuno dei due ha la lingua almeno di quel regno almeno all'11, ognuno vedrà il proprio livello di caratteri strani. Se invece almeno uno dei due ha la lingua dall'11 al 23, quello con livello più basso vedrà caratteri latini randomici. Se infine uno dei due ha le lingue dal 24 in su, anche l'altro sarà in grado di capire esattamente quanto l'interlocutore.
