# Aree

Le aree possono essere suddivise in tre parti: regni, neutrali e dungeon

## Regni

Tre regni con medesime funzioni (NPC, centro città ecc.) ma diversa struttura geografica (la mappa è diversa). Sono la zona con i mostri di livello più basso dell'intero gioco (1-14), nonché la zona iniziale per ogni giocatore. Se dovessimo disegnare una mappa globale, essa sarebbe una sorta di triangolo con un regno per vertice.

## Zone neutrali

Le zone neutrali sono invece circondate dai tre regni e i mostri qui presenti notevolmente più forti. Ogni regno è collegato con le stesse due zone neutrali, ovvero La Valle* e il Deserto*. Esse sono anche le zone neutrali più facili. Segue una lista ATTUALMENTE NON COMPLETA:
- La Valle*
- Deserto*
- Naos dell'Abbandono
- Altopiano*
- Cime dei tre soli*

*Nomi provvisori  

La progressione delle mappe è
- Valle -> Naos -> Altopiano -> ??? -> Cime
- Deserto -> ??? (ecc.)

> La progressione delle mappe non è lineare, ma finché non saranno definite meglio è inutile disegnare uno schema dettagliato

## Dungeon

I dungeon sono minimappe disponibili in alcune aree. Al contrario di molti giochi online, NON tutti i dungeon sono istanziati. In altre parole, entrando in un dungeon vedrete tutti gli altri giocatori che come voi sono dentro.  

Lista dei dungeon ATTUALMENTE NON COMPLETA:
- Grotta delle scimmie* (regni)
- Torre dei demoni* (Valle)

*Nomi provvisori

> Per chi viene da Metin: questi nomi servono per dare l'idea della loro funzione, ovvero la grotta per ottenere il cavallo e la torre per "scalare", seppur in quest'ultimo caso la meccanica verrà rivista

